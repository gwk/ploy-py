#!/usr/bin/env sh

set -ex

mkdir -p _bld

py3 -B bld/__main__.py bld/bld.bld
