# Copyright 2015 George King.
# Permission to use this file is granted in ploy/license.txt.

import os
import os.path as _path
import stat
import sys
import zipfile

from base import *


args = sys.argv[1:]
src_paths = args[:-1]
checkS(src_paths, 'usage: src_paths... out_path')
out_path = args[-1]

found_main = False

for path in src_paths:
  checkS(_path.exists(path), "source does not exist:", path)
  name = _path.basename(path)
  if name == '__main__.py':
    found_main = True

checkS(found_main, 'src paths must include __main__.py')

f = open(out_path, 'wb')
f.write(b'#!/usr/bin/env python3\n')

with zipfile.ZipFile(f, 'w') as z:
  for path in src_paths:
    dir = _path.dirname(path) + '/'
    for p in walk_all_paths_dirs(path, exts=['.py']):
      n = _path.basename(path)
      if n == '__pycache__': continue
      archive_p = path_rel_to(dir, p)
      logSL(p, '->', archive_p)
      z.write(p, archive_p)
      continue
      try:
        z.write(p, archive_p)
      except UserWarning as e:
        failS("zip error", e)

fd = f.fileno()
mode = os.stat(fd).st_mode | stat.S_IEXEC
os.chmod(fd, mode)

