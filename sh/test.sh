# © 2015 George King.
# Permission to use this file is granted in ploy/license.txt.

set -e

cd $(dirname $0)/..

sh/run.sh test/0-basic/ret-0.ploy
PYTHONPATH=. tools/test.py -compiler 'python3 boot lib' "$@"

