# © 2015 George King.
# Permission to use this file is granted in ploy/license.txt.

set -e
cd $(dirname $0)/..
PYTHONPATH=. python3 boot/parse.py "$@"
