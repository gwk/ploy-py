# © 2015 George King.
# Permission to use this file is granted in ploy/license.txt.

import sys
from Syn import *


def is_char_sym_head(char:Char) -> Bool:
  return char.isalpha()

def is_char_sym_tail(char:Char) -> Bool:
  return char.isalpha() or char.isdigit() or char == '-'

def is_char_terminator(char:Char) -> Bool:
  return char in ')>]};# \n'

def parse_fail(src:Src, pos:Pos, last:Opt(Pos), fmt:Str, *items:Obj):
  log_src_pos(src, pos, last, 'parse error', fmt, *items)
  sys.exit(1)


def parse_fail_syn(syn:Syn, fmt:Str, *items:Obj):
  parse_fail(syn.src, syn.pos, syn.last, fmt, *items)


def parse_space(src:Src, pos:Pos) -> Pos:
  p = pos
  in_comment = False
  while src_has_some(src, p):
    c = src_char(src, p)
    if c == '\n':
      p = pos_adv_line(p)
      in_comment = False
    elif in_comment:
      p = pos_adv(p)
    elif c == ' ':
      p = pos_adv(p)
    elif c == '#':
      p = pos_adv(p)
      in_comment = True
    else:
      break
  return p


def parse_sym_val(src:Src, pos:Pos) -> Tuple(Str, Pos, Pos):
  '''
  returns last-pos, val.
  note: unlike other parse functions, this returns a plain Str instead of Syn.
  (the string is classified by the caller).
  '''
  # initial char already classified.
  p = pos
  while src_has_next(src, p):
    if not is_char_sym_tail(src_next(src, p)):
      break
    p = pos_adv(p)
  last = p
  end = pos_adv(last)
  return src_slice(src, pos, end), last, parse_space(src, end)


def parse_num(src:Src, pos:Pos, found_dot:Bool) -> Syn:
  # initial char already recognized.
  p = pos
  while src_has_next(src, p):
    c = src_next(src, p)
    if c == '.':
      if found_dot:
        parse_fail(src, pos, p, "repeated '.' in number literal")
      found_dot = True
    elif not c.isdigit():
      break
    p = pos_adv(p)
  last = p
  end = pos_adv(last)
  slice = src_slice(src, pos, end)
  # TODO: handle bare '.' and '-'.
  if found_dot:
    kind = 'Flt'
    val = Flt(slice)
  else:
    kind = 'Int'
    val = Int(slice)
  return Syn(kind, src, pos=pos, last=last, end=parse_space(src, end), val=val)


def parse_str(src:Src, pos:Pos, terminator:Char) -> Syn:
  chars = []
  escape = False
  escapePos = None
  escapeCount = 0
  escapeOrdinal = 0
  p = pos_adv(pos)
  while True:
    if not src_has_some(src, p):
      parse_fail(src, pos, p, 'unterminated string literal')
    c = src_char(src, p)
    if escape:
      escape = False
      e = c
      if c == '\\': pass # backslash.
      elif c == "'": pass # single quote.
      elif c == '"': pass # double quote.
      elif c == '0': e = '\0' # null terminator.
      elif c == 'b': e = '\b' # backspace.
      elif c == 'n': e = '\n' # line feed.
      elif c == 'r': e = '\r' # carriage return.
      elif c == 't': e = '\t' # horizontal tab.
      elif c == 'x': escapeCount = 2; e = None
      elif c == 'u': escapeCount = 4; e = None
      elif c == 'U': escapeCount = 6; e = None
      else: chars.append('\\') # not a valid escape code, so add the leading backslash.
      if e is not None:
        chars.append(e)
    elif escapeCount:
      if c not in '01234567890abcdefABCDEF':
        parse_fail(src, p, None, 'escape ordinal must be a hexadecimal digit')
      escapeOrdinal = escapeOrdinal * 16 + int(c, base=16)
      escapeCount -= 1
      if escapeCount == 0:
        if escapeOrdinal > 0x10FFFF:
          parse_fail(src, escapePos, p, 'escape ordinal exceeds unicode maximum of 0x10FFFF')
        chars.append(chr(escapeOrdinal))
    else:
      if c == terminator:
        break
      elif c == '\\':
        escape = True
        escapePos = p
        escapeCount = 0
        escapeOrdinal = 0
      else:
        chars.append(c)
    p = pos_adv(p)
  last = p
  end = pos_adv(last)
  val = ''.join(chars)
  return Syn('Str', src, pos=pos, last=last, end=parse_space(src, end), val=val)


prefixes = (
  ('ByVal',   '$', True),
  ('Deref',   '@', False),
  ('Qua',     '~', False),
  ('Quo',     '`', False),
  ('Unq',     ',', False),
  ('Variad',  '&', False),
)


def parse_prefix(src:Src, pos:Pos) -> Opt(Syn):
  for kind, pre, is_ok_alone in prefixes:
    if src_has_str(src, pos, pre):
      last = pos_adv(pos, len(pre) - 1)
      p = pos_adv(last)
      c = src_char(src, p)
      if is_char_terminator(c):
        if is_ok_alone:
          end = parse_space(src, p)
          return Syn('Sym', src, pos=pos, last=p, end=end, val=pre)
        else:
          parse_fail(src, pos, p, "dangling prefix '{}'", pre)
      sub = parse_phrase(src, p)
      return Syn(kind, src, pos=pos, last=sub.last, end=sub.end, subs=(sub,))
  return None


sentences = (
  ('Agg',       '(', ')'),
  ('AggExpand', '[', ']'),
  ('Tup',       '<', '>'),
  ('Do',        '{', '}'),
)

def parse_sentence(src:Src, pos:Pos) -> Opt(Syn):
  for kind, op, cl in sentences:
    if src_has_str(src, pos, op):
      p = pos_adv(pos, len(op))
      subs, p = parse_exprs(src, p)
      if not src_has_str(src, p, cl):
        parse_fail(src, pos, p, "'{}' form expects terminator: '{}'", kind, cl)
      last = pos_adv(p, len(cl) - 1)
      end = parse_space(src, pos_adv(last))
      return Syn(kind, src, pos=pos, last=last, end=end, subs=subs)
  return None


def parse_kw_adj(src:Src, pos:Pos, pos_subs:Pos, kind:Str) -> Syn:
  sub = parse_phrase(src, pos)
  if is_a(sub, Syn_Err): return sub
  return Syn(kind, src, pos=pos, last=sub.last, end=sub.end, subs=(sub,))


def parse_kw_sentence(src:Src, pos:Pos, pos_subs:Pos, kind:Str, keyword:Str) -> Syn:
  subs, p = parse_exprs(src, pos_subs)
  cl = ';'
  if not src_has_str(src, p, cl):
    parse_fail(src, pos, p, "`{}` keyword form expects terminator: `{}`", keyword, cl)
  last = pos_adv(p, len(cl) - 1)
  end = parse_space(src, pos_adv(last))
  return Syn(kind, src, pos=pos, last=last, end=end, subs=subs)


keywords = {
  'pub'     : (parse_kw_adj, 'Pub'),
  'scoped'  : (parse_kw_adj, 'Scoped'),
  'enum'    : (parse_kw_sentence, 'Enum'),
  'fn'      : (parse_kw_sentence, 'Fn'),
  'if'      : (parse_kw_sentence, 'If'),
  'in'      : (parse_kw_sentence, 'In'),
  'struct'  : (parse_kw_sentence, 'Struct'),
  'PLOY-HOST' : (parse_kw_sentence, 'HostDecl'),
}


def parse_sym_or_keyword(src:Src, pos:Pos) -> Syn:
  val, last, end = parse_sym_val(src, pos)
  try:
    f, kind = keywords[val]
  except KeyError:
    pass
  else:
    return f(src, pos, end, kind, val)
  # regular sym.
  return Syn('Sym', src, pos=pos, last=last, end=end, val=val)


def parse_poly(src:Src, pos:Pos) -> Syn:
  '''
  the 'polymorphic' parsing function; classifies expression by leading character and dispatches.
  '''
  c = src_char(src, pos)
  if is_char_sym_head(c):
    return parse_sym_or_keyword(src, pos)
  if c.isdigit() or c == '-':
    return parse_num(src, pos, found_dot=False)
  if c == '.':
    return parse_num(src, pos, found_dot=True)
  if c == '"' or c == "'":
    return parse_str(src, pos, c)
  r = parse_prefix(src, pos)
  if r: return r
  r = parse_sentence(src, pos)
  if r: return r
  parse_fail(src, pos, pos, "unexpected character: {}", repr(c))


operator_groups = (
  ( ('Alias', ':='),
    ('Bind', '='),),
  ( ('Ann', ':'),),
  ( ('Acc', '@'),
    ('Call', '.'),
    ('Case', '?'),
    ('Expand', '|'),
    ('Reify', '^'),
    ('Sig', '%'), ),
)

adjacency_operators = (
  ('CallAdj', '('),
  ('ExpandAdj', '['),
  ('ReifyAdj', '<'),
)

def parse_phrase(src:Src, pos:Pos, precedence:Int=0) -> Syn:
  # parse a series of right-recursive operators.
  expr = parse_poly(src, pos)
  p = expr.end
  if src_has_some(src, expr.end):
    for i in range(precedence, len(operator_groups)):
      for kind, op in operator_groups[i]:
        if src_has_str(src, p, op):
          p = pos_adv(p, len(op))
          p = parse_space(src, p)
          right = parse_phrase(src, p, precedence=i)
          return Syn(kind, src, expr.pos, last=right.last, end=right.end, subs=(expr, right))
    # 'adjacency operators' have highest precedence.
    if not syn_has_space(expr):
      for k, open_c in adjacency_operators:
        if src_char(src, p) == open_c:
          right = parse_phrase(src, p, precedence=i)
          return Syn(k, src, expr.pos, last=right.last, end=right.end, subs=(expr, right))
  return expr


def parse_exprs(src:Src, pos:Pos) -> Tuple(Array(Syn), Pos):
  exprs = []
  p = parse_space(src, pos)
  prev = None
  while src_has_some(src, p):
    c = src_char(src, p)
    if is_char_terminator(c):
      break
    expr = parse_phrase(src, p)
    if prev and not syn_has_space(prev):
      parse_fail(src, p, None, 'adjacent expressions require separating space')
    exprs.append(expr)
    p = expr.end
    prev = expr
  return tuple(exprs), p


def parse_src(src:Src) -> Syn:
  err = None
  pos = Pos(0, 0, 0)
  p = parse_space(src, pos)
  exprs, end = parse_exprs(src, p)
  if src_has_some(src, end):
    c = src_char(src, end)
    return parse_fail(src, end, None, "unexpected terminator character: '{}'", c)
  last = exprs[-1].last if exprs else pos
  return Syn('File', src, pos, last=last, end=end, subs=exprs)


def parse_path(path:Str) -> Syn:
  with open(path) as f:
    text = f.read()
  src = Src(path, text)
  return parse_src(src)


if __name__ == '__main__':
  path = sys.argv[1]
  with open(path) as f:
    src = Src(path, f.read())
  syn = parse_src(src)
  # success.
  write_Syn(stdout, syn)


check_module()
