# © 2015 George King.
# Permission to use this file is granted in ploy/license.txt.

from Pos import *


# source file path and file contents.
def_struct('Src', 'path:Str text:Str')

def src_repr(src:Src) -> Str:
  return 'Src({})'.format(src.path)

Src.__repr__ = src_repr


def src_has_some(src:Src, pos:Pos, ahead:Int=0) -> Bool:
  return pos.idx + ahead < len(src.text)

def src_has_next(src:Src, pos:Pos) -> Bool:
  return src_has_some(src, pos, ahead=1)

def src_has_str(src:Src, pos:Pos, str:Str) -> Bool:
  return src.text.find(str, pos.idx, pos.idx + 1) == pos.idx

def src_char(src:Src, pos:Pos, ahead:Int=0) -> Char:
  return src.text[pos.idx + ahead]

def src_next(src:Src, pos:Pos) -> Char:
  return src_char(src, pos, ahead=1)

def src_slice(src:Src, pos:Pos, end:Pos) -> Str:
  return src.text[pos.idx:end.idx]

def src_line(src:Src, pos:Pos) -> Str:
  'return the line of source text containing pos; always excludes newline for consistency.'
  b = pos.idx
  while b > 0:
    p = b - 1
    if src.text[p] == '\n':
      break
    b = p
  e = pos.idx
  while e < len(src.text):
    if src.text[e] == '\n':
      break
    e += 1
  return src.text[b:e]


def pos_underline(pos:Pos, last:Opt(Pos)) -> Str:
  indent = ' ' * pos.col
  if last is None: # zero length.
    return indent + '^'
  else:
    assert pos.line == last.line
    return indent + '~' * (1 + last.col - pos.col)


def pos_underlines(pos:Pos, last:Pos, pos_line:Str) -> Tuple(Str, Str):
  assert pos.line < last.line
  return (
    '{}{}-'.format(' ' * pos.col, '~' * (len(pos_line) - pos.col)),
    '~' * (1 + last.col))


def log_src_pos(src:Src, pos:Pos, last:Opt(Pos), prefix:Str, fmt:Str, *items:Obj):
  line = src_line(src, pos)
  msg = fmt.format(*items)
  if last is None:
    logFL('{}: {}:{}:{}: {}\n  {}\n  {}', prefix, src.path, pos.line + 1, pos.col, msg, line,
      pos_underline(pos, None))
  elif pos.line == last.line:
    columns = '{}-{}'.format(pos.col + 1, last.col + 1) if pos.col != last.col else str(pos.col + 1)
    logFL('{}: {}:{}:{}: {}\n  {}\n  {}', prefix, src.path, pos.line + 1, columns, msg, line,
      pos_underline(pos, last))
  else:
    line_l = src_line(src, last)
    ulp, ull = pos_underlines(pos, last, line)
    logFL('{} from: {}:{}:{}:\n  {}\n  {}', prefix, src.path, pos.line + 1, pos.col + 1, line,
      ulp)
    logFL('to: {}:{}:{}: {}\n  {}\n  {}', src.path, last.line + 1, last.col + 1, msg, line_l,
      ull)


check_module()
