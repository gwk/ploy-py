# © 2015 George King.
# Permission to use this file is granted in ploy/license.txt.

from common.all import *
from scope import *


# Em.

class Em:
  def __init__(self):
    self.lines = [] # List(Str).


def em_add(self:Em, depth:Int, string:Str):
  self.lines.append('  ' * depth + string)

def em_fmt(self:Em, depth:Int, fmt:Str, *items:Obj):
  self.lines.append('  ' * depth + fmt.format(*items))

def em_append(self:Em, string:Str):
  self.lines[-1] += string

Em.add = em_add
Em.fmt = em_fmt
Em.append = em_append


# emit helpers.

def emit_Body(em:Em, depth:Int, scope:Scope, body:Body) -> TypeVal:
  for stmt in body.stmts:
    emit(stmt, em, depth, scope)
  if body.tail is None:
    return tv_Nil
  else:
    em.fmt(depth, 'return')
    return emit(body.tail, em, depth, scope)


# emit.

@singledispatch
def emit(expr:AnyExpr, em:Em, depth:Int, scope:Scope) -> TypeVal:
  raise TypeError('emit not implemented for expression type: {}'.format(type(expr)))


@emit.register(Agg)
def _(agg:Agg, em:Em, depth:Int, scope:Scope) -> TypeVal:
  em.add(depth, '{')
  pars = []
  for i, arg in enumerate(agg.fields):
    name = (arg.name if arg.name is not None else i)
    em.fmt(depth, '{}{}:', (', ' if i else '  '), name)
    type = emit(arg.expr, em, depth + 1, scope)
    pars.append(TVPar(name=name, type=type, dflt=None))
  em.append('}')
  return TVTup(pars=tuple(pars))


def moduleBindRequiresLazy(bind:Bind) -> Bool:
  return not isinstance(bind.expr, (Fn, Int, Str))


@emit.register(Bind)
def _(bind:Bind, em:Em, depth:Int, scope:Scope) -> TypeVal:
  if isModuleScope(scope) and moduleBindRequiresLazy(bind):
    assert depth == 0
    em.fmt(depth, 'var {}__lazy = undefined', bind.name)
    em.fmt(depth, 'const {}__acc = function() {{', bind.name)
    tv = emit(bind.expr, em, depth + 1, scope)
    em.append('}')
  else:
    em.fmt(depth, 'const {} =', bind.name)
    tv = emit(bind.expr, em, depth + 1, scope)
    em.append(';')
  addRecord(scope, sym=sym, isType=isType, isLazy=isLazy, typeVal=typeVal)
  return tvNil


@emit.register(Call)
def _(call:Call, em:Em, depth:Int, scope:Scope) -> TypeVal:
  em.add(depth, '((')
  tvCallee = emit(call.callee, em, depth + 1, scope)
  em.append(')(')
  tvArg = emit(call.arg, em, depth + 1, scope)
  em.append('))')
  typeCheck(tvArg, tvCallee.arg)
  return TODO


@emit.register(Fn)
def _(fn:Fn, em:Em, depth:Int, scope:Scope) -> TypeVal:
  em.add(depth, 'function($){')
  emit_Body(em, depth + 1, scope, fn.body)
  em.append('}')
  return TODO

@emit.register(HostDecl)
def _(hostDecl:HostDecl, em:Em, depth:Int, scope:Scope) -> TypeVal:
  tv = tvForType(hostDecl.type, scope)
  addRecord(scope, sym=hostDecl.name, isType=True, isLazy=False, typeVal=tv)
  return tvNil


@emit.register(Int)
def _(expr:Int, em:Em, depth:Int, scope:Scope) -> TypeVal:
  em.fmt(depth, '{}', expr)
  return TODO


@emit.register(Str)
def _(expr:Str, em:Em, depth:Int, scope:Scope) -> TypeVal:
  em.fmt(depth, '{!r}', expr) # TODO: fix discrepency between py and js string literals.
  return TODO


@emit.register(Sym)
def _(sym:Sym, em:Em, depth:Int, scope:Scope) -> TypeVal:
  em.add(depth, sym.name)
  return recordForSym(scope, sym, isType=False).typeVal



def emit_module_in(em:Em, module_in:In):
  for stmt in module_in.stmts:
    emit(stmt, em, 0, pushScope(rootScope))


def emit_main(em:Em, main:Main) -> TypeVal:
  em.add(0, 'process.exit(function() { // main.')
  tv = emit_Body(em, 0, pushScope(rootScope), main.body)
  checkType(tv, tvInt)
  em.add(0, '}()) // END main.')


def emit_program(f:TextFile, module_ins:Array(In), main:Main):
  em = Em()
  em.add(0, '#!/usr/bin/env iojs')
  em.add(0, '"use strict";\n')

  em.add(0, 'function() { // ploy namespace.')
  em.add(0, '// host.js.\n')
  for line in open('boot/host/host.js'):
    assert line[-1] == '\n'
    em.add(0, line[:-1]) # strip newline.
  em.add(0, '// END host.js.\n')

  for m in module_ins:
    emit_module_in(em, m)
  emit_main(em, main)
  em.add(0, '}() // END ploy namespace.')

  for l in em.lines:
    f.write(l)
    f.write('\n')


check_module()
