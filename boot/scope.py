# © 2015 George King.
# Permission to use this file is granted in ploy/license.txt.

from TypeVal import *


def_struct('ScopeRecord', 'isType:Bool isLazy:Bool typeVal:TypeVal syn:Opt(Syn)')
# ScopeRecord maps both bindings and typealiases into the same namespace.
# if isType is set, then the record represents the type of a TypeExpr; otherwise of a ValExpr.
# if isLazy is set, then the record represents a module binding that is accessed through a call to generated wrapper function, which performs lazy initialization.

_Scope = Fwd('Scope')
def_struct('Scope', 'parent:Opt(_Scope) records:Dict(Str,ScopeRecord)')


def addRecord(scope:Scope, sym:Sym, isType:Bool, isLazy:Bool, typeVal:TypeVal):
  if sym.name in scope.records:
    fail_syn2(syn, scope.records[sym.name].syn,
      'redefinition for name: {}', 'previous definition is here', syn.name)
  scope.records[sym.name] = ScopeRecord(isType=isType, isLazy=isLazy, typeVal=typeVal, syn=sym.syn)


def recordForSym(scope:Scope, sym:Sym, isType:Bool) -> ScopeRecord:
  try:
    record = scope.records[sym.name]
  except KeyError:
    pass
  else:
    if isType != record.isType:
      fail_syn2(sym.syn, record.syn, 'expected a {}; name represents a {}', 'defined here',
        'type' if isType else 'value', 'value' if isType else 'type')
    return record
  if scope.parent is None:
    fail_syn(sym.syn, 'name is not defined in this scope: {}', sym.name)
  else:
    return recordForSym(scope.parent, sym, isType)


@singledispatch
def tvForType(expr:TypeExpr, scope:Scope) -> TypeVal:
  raise TypeError('tvForType not implemented for type expression type: {}'.format(type(expr)))


@tvForType.register(Reify)
def _(sym:Sym, scope:Scope) -> TypeVal:
  return TODO


@tvForType.register(Sig)
def _(sig:Sig, scope:Scope) -> TypeVal:
  return TVSig(par=tvForType(sig.par, scope), ret=tvForType(sig.ret, scope))


@tvForType.register(Sym)
def _(sym:Sym, scope:Scope) -> TypeVal:
  return recordForSym(scope, sym, isType=True).typeVal


@tvForType.register(Tup)
def _(tup:Tup, scope:Scope) -> TypeVal:
  pars = []
  for i, p in enumerate(tup.fields):
    if isinstance(p, DfltPar):
      n = p.name.val
      t = tvForType(p.dflt, scope)
      d = p.dflt
    elif isinstance(p, NamedPar):
      n = p.name.val
      t = p.type
      d = None
    else: # plain type.
      n = str(i)
      t = tvForType(p, scope)
      d = None
    pars.append(TVPar(name=n, type=t, dflt=d))
  return TVTup(pars=tuple(pars))


tvNil = TVTup(pars=())
tvBool = TVPrim(name='Bool')
tvFileDescriptor = TVPrim(name='FileDescriptor')
tvInt = TVPrim(name='Int')
tvStr = TVPrim(name='Str')

rootScope = Scope(parent=None,
  records={ tv.name : ScopeRecord(isType=True, isLazy=False, typeVal=tv, syn=None) for tv in [
  tvBool,
  tvInt,
  tvStr,
]})


def isModuleScope(scope:Scope) -> Bool:
  return scope.parent is rootScope


def pushScope(scope:Scope) -> Scope:
  return Scope(parent=scope, records={})


check_module()
