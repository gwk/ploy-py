# © 2015 George King.
# Permission to use this file is granted in ploy/license.txt.

from common.all import *


# character index, line and column numbers (all 0-indexed).
def_struct('Pos', 'idx:Int line:Int col:Int=0')


def pos_adv(pos:Pos, dist:Int=1) -> Pos:
  idx, line, col = pos
  return Pos(idx + dist, line, col + 1)

def pos_adv_line(pos:Pos) -> Pos:
  idx, line, col = pos
  return Pos(idx + 1, line + 1, 0)

def pos_dist(pos:Pos, end:Pos) -> Int:
  return end.idx - pos.idx


check_module()
