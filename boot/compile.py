# © 2015 George King.
# Permission to use this file is granted in ploy/license.txt.

from sem import *


def check_kind(syn:Syn, kind:Str, noun:Str, exp_str:Str=''):
    check_syn(syn, syn.kind == kind, '{} expects {}; received {}', noun, exp_str or kind, syn.kind)


def check_subs(syn:Syn, len_exp:Int, noun:Str) -> Array(Syn):
  l = len(syn.subs)
  check_syn(syn, l == len_exp, '{} expects {} subexpression{}; received {}',
    noun, len_exp, plural_s(len_exp), l)
  return syn.subs


def check_sub(syn:Syn, noun:Str) -> Syn:
  return check_subs(syn, 1, noun)[0]


def check_pair(syn:Syn, noun:Str) -> Array(Syn):
  return check_subs(syn, 2, noun)


def check_head(syn:Syn, len_min:Int, msg:Str) -> Array(Syn):
  l = len(syn)
  check_syn(syn, l >= len_min, msg)
  return syn.subs[:len_min]


# common.

def compile_Sym(syn:Syn) -> Sym:
  assert syn.kind == 'Sym'
  return Sym(syn, name=syn.val)


# Par.

def compile_Par(syn:Syn, index:Int) -> Par:
  if syn.kind == 'Bind': # parameter is a default statement.
    name_syn, dflt_syn = check_pair(name_dflt, 'parameter')
    check_kind(name, 'Sym', 'parameter name')
    return DfltPar(syn, name=compile_Sym(name), dflt=compile_val(dflt_syn))
  elif syn.kind == 'Ann': # parameter is a type annotation.
    name_syn, type_syn = check_pair(syn, 'annotated parameter')
    check_kind(name, 'Sym', 'parameter name')
    return NamedPar(syn, name=compile_Sym(name_syn), type=compile_type(type_syn))
  else: # plain type.
    return compile_type(syn)


def compile_Pars(syns:Array(Syn)) -> Array(Par):
  return tuple(compile_Par(s, i) for i, s in enumerate(syns))


# TypeExpr.

def compile_Reify(syn:Syn) -> Reify:
  callee, arg = check_pair(syn, 'type reification')
  return Reify(syn, callee=compile_type(callee), arg=compile_type(arg))


def compile_Sig(syn:Syn) -> Sig:
  par, ret = check_pair(syn, 'type signature')
  return Sig(syn, par=compile_type(par), ret=compile_type(ret))


def compile_Tup(syn:Syn) -> Tup:
  return Tup(syn, fields=compile_Pars(syn.subs))


compile_type_dispatch = {
  'Reify'     : compile_Reify,
  'ReifyAdj'  : compile_Reify,
  'Sig'       : compile_Sig,
  'Sym'       : compile_Sym,
  'Tup'       : compile_Tup,
}


def compile_type(syn:Syn) -> TypeExpr:
  try:
    f = compile_type_dispatch[syn.kind]
  except KeyError:
    fail_syn(syn, 'syntax kind does not represent a type expression: {}', syn.kind)
  return f(syn)


# ValExpr component types.

def compile_Arg(syn:Syn) -> Arg:
  if syn.kind == 'Bind': # named argument.
    name_syn, expr_syn = check_pair(syn, 'named argument')
    check_kind(name_syn, 'Sym', noun='argument name')
    name = compile_Sym(name_syn)
    expr = compile_val(expr_syn)
  # TODO: variad.
  else:
    name = None
    expr = compile_val(syn)
  return Arg(syn, name=name, expr=expr)


def compile_Case(syn:Syn) -> Case:
  condition, consequence = check_pair(syn, 'case')
  return Case(syn, condition=compile_val(condition), consequence=compile_val(consequence))


def compile_Body(syns:Array(Syn)) -> Body:
  stmts = []
  tail = None
  last_i = len(syns) - 1
  for i, syn in enumerate(syns):
    if i == last_i and syn.kind in ValExpr_kinds : # last expression is an expr.
      tail = compile_val(syn)
    else:
      stmts.append(compile_stmt(syn))
  return Body(stmts=tuple(stmts), tail=tail)


# ValExpr.

def compile_Acc(syn:Syn) -> Acc:
  accessor, expr = check_pair(syn, 'access')
  check_kind(accessor, Sym, 'accessor')
  return Acc(syn, accessor=accessor, expr=compile_val(expr))


def compile_Agg(syn:Syn) -> Agg:
  return Agg(syn, fields=tuple(compile_Arg(s) for s in syn.subs))


def compile_Ann(syn:Syn) -> Ann:
  expr, type_ = check_pair(syn, 'type annotation')
  return Ann(syn, expr=compile_val(expr), type=compile_type(type_))


def compile_Call(syn:Syn) -> Call:
  callee, arg = check_pair(syn, 'call')
  return Call(syn, callee=compile_val(callee), arg=compile_val(arg))


def compile_Do(syn:Syn) -> Do:
  return Do(syn, body=compile_Body(syn.subs))


def compile_Fn(syn:Syn) -> Fn:
  sig_syn, = check_head(syn, 1, '`fn` requires a signature.')
  return Fn(syn, sig=compile_type(sig_syn), body=compile_Body(syn.subs[1:]))


def compile_If(syn:Syn) -> If:
  cases = []
  dflt = None
  last_i = len(syn.subs) - 1
  for i, e in enumerate(syn.subs):
    if i == last_i and e.kind != 'Case':
      dflt = compile_val(e)
    else:
      cases.append(compile_Case(e))
  return If(syn, tuple(cases), dflt)


def compile_Int(syn:Syn) -> Int:
  return syn.val


def compile_Scoped(syn:Syn) -> Scoped:
  sub = check_sub(syn, '`scoped`')
  return Scoped(syn, expr=compile_val(sub))


def compile_Str(syn:Syn) -> Str:
  return syn.val


compile_val_disp = {
  'Acc'     : compile_Acc,
  'Agg'     : compile_Agg,
  'Ann'     : compile_Ann,
  'Call'    : compile_Call,
  'CallAdj' : compile_Call,
  'Do'      : compile_Do,
  #'Flt'     : None,
  'Fn'      : compile_Fn,
  'If'      : compile_If,
  'Int'     : compile_Int,
  #'Quo'     : None,
  'Scoped'  : compile_Scoped,
  'Str'     : compile_Str,
  'Sym'     : compile_Sym,
}

ValExpr_kinds = set(compile_val_disp.keys())


def compile_val(syn:Syn) -> ValExpr:
  try:
    f = compile_val_disp[syn.kind]
  except KeyError:
    fail_syn(syn, 'syntax kind does not represent a value expression: {}', syn.kind)
  return f(syn)


def compile_vals(syns:Array(Syn)) -> Array(ValExpr):
  return tuple(compile_val(syn) for syn in syns)


# Stmt.

def compile_Alias(syn:Syn) -> Alias:
  name_syn, type_ = check_pair(syn, 'type alias')
  check_kind(name_syn, 'Sym', 'type alias name')
  return Alias(syn, name=compile_Sym(name_syn), type=compile_type(type_))


def compile_Bind(syn:Syn) -> Bind:
  name_syn, expr = check_pair(syn, 'bind')
  check_kind(name_syn, 'Sym', 'binding name')
  return Bind(syn, name=compile_Sym(name_syn), expr=compile_val(expr))


def compile_Enum(syn:Syn) -> Enum:
  name_syn, = check_head(syn, 1, '`enum` requires a name.')
  check_kind(name_syn, 'Sym', 'enum name')
  variants = compile_Pars(tuple(syn.subs[1:]))
  return Enum(syn, name=compile_Sym(name_syn), variants=variants)


def compile_Struct(syn:Syn) -> Struct:
  name_syn, = check_head(syn, 1, '`struct` requires a name.')
  check_kind(name, 'Sym', 'name')
  fields = help_compile_Pars(tuple(syn.subs[1:]))
  return Struct(syn, name=compile_Sym(name_syn), fields=fields)


compile_stmt_dispatch = {
  'Alias'   : compile_Alias,
  'Bind'    : compile_Bind,
  'Call'    : compile_Call,
  'CallAdj' : compile_Call,
  'Do'      : compile_Do,
  'Enum'    : compile_Enum,
  'If'      : compile_If,
  'Scoped'   : compile_Scoped,
  'Struct'  : compile_Struct,
}

def compile_stmt(syn:Syn) -> Stmt:
  try:
    f = compile_stmt_dispatch[syn.kind]
  except KeyError:
    fail_syn(syn, 'syntax kind does not represent a statement: {}', syn.kind)
  return f(syn)


# ModuleStmt.

def compile_Pub(syn:Syn) -> Pub:
  sub = check_sub(syn, '`pub`')
  check_syn(sub, sub.kind != 'Pub', 'nested `pub` statements')
  return Pub(syn, stmt=compile_module_stmt(sub))


def compile_HostDecl(syn:Syn) -> HostDecl:
  name_syn, type = check_pair(syn, '`PLOY-HOST`')
  check_kind(name_syn, 'Sym', 'host declaration', 'name')
  return HostDecl(syn, name=compile_Sym(name_syn), type=compile_type(type))


compile_module_stmt_dispatch = {
  'Alias'   : compile_Alias,
  'Bind'    : compile_Bind,
  'Enum'    : compile_Enum,
  'Pub'     : compile_Pub,
  'Struct'  : compile_Struct,
  'HostDecl' : compile_HostDecl,
}

def compile_module_stmt(syn:Syn) -> ModuleStmt:
  try:
    f = compile_module_stmt_dispatch[syn.kind]
  except KeyError:
    fail_syn(syn, 'syntax kind does not represent a module-level statement: {}', syn.kind)
  return f(syn)


# modules.

def compile_In(syn:Syn) -> In:
  check_kind(syn, 'In', 'module', '`in` statements')
  name_syn, = check_head(syn, 1, '`in` requires a module name.')
  check_kind(name_syn, 'Sym', 'module path.')
  return In(syn, name=compile_Sym(name_syn),
    stmts=tuple(compile_module_stmt(s) for s in syn.subs[1:]))


def compile_modules(syns:Array(Syn)) -> Array(In):
  return tuple(compile_In(s) for s in syns)


# main.

def compile_main(syn:Syn) -> Main:
  for syn in syn.subs: # extra reporting to make the semantics of `in` more clear.
    check_syn(syn, syn.kind != 'In', '`in` statement is invalid within a main file')
  return Main(syn, body=compile_Body(syn.subs))


# file.

def compile_File(syn:Syn) -> Union(Array(In), Main):
  assert syn.kind == 'File'
  subs = syn.subs
  # cannot tell the difference between main and empty module files.
  # for simplicity, assume it is an empty module file; an empty main becomes an error.
  if len(subs) == 0 or subs[0].kind == 'In':
    return compile_modules(subs)
  else:
   return compile_main(syn)


check_module()

