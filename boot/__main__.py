
#!/usr/bin/env python3.4
# © 2015 George King.
# Permission to use this file is granted in ploy/license.txt.

import sys

import parse
import sem
import compile
import emit

from common.all import *


def parse_and_compile_path(path:Str) -> Union(Array(sem.In), sem.Main):
  syn = parse.parse_path(path)
  return compile.compile_File(syn)


def main():
  
  host_dir = path_join(sys.argv[0], 'host')

  args = sys.argv[1:]
  src_paths = []
  out_path = None
  i = 0
  while i < len(args):
    a = args[i]
    if a == '-o':
      i += 1
      check(i < len(args), 'dangling -o flag.')
      out_path = args[i]
    else:
      src_paths.append(a)
    i += 1

  for p in src_paths:
    checkF(is_dir(p) or path_ext(p) == '.ploy',
      'error: source must have .ploy extension; received: {}', p)

  host = parse_and_compile_path('boot/host/host.ploy')
  checkF(not isinstance(host, sem.Main), 'error: host.ploy is not a module file.')

  _modules = list(host)
  main_path = None
  main_obj = None
  for path in walk_all_files(*src_paths, exts=['.ploy']):
    m = parse_and_compile_path(path)
    if isinstance(m, sem.Main):
      checkF(main_path is None, 'previously found main file: {}\n  duplicate main: {}', main_path, path)
      main_path = path
      main_obj = m
    else:
      _modules.extend(m)
  modules = tuple(_modules)

  check(main_path, 'no main path')

  d, name, ext = split_dir_ext(main_path)
  dst_dir = path_join('_bld', d)
  dst_path = path_join(dst_dir, name)

  tmp_path = dst_path + '.tmp'
  dst_c = dst_path + '.c'
  tmp_c = dst_c + '.tmp'

  if out_path is None:
    out_path = dst_path

  make_dirs(dst_dir)

  target = 'js'

  if target == 'c':
    with open(tmp_c, 'w') as f:
      raiseF('emit for c target not implemented')
      #emit.emit_program(f, modules, main_obj)
    move_file(tmp_c, dst_c, overwrite=True)
    cmd = ['clang', '-I' + host_dir, dst_c, '-o', tmp_path]
    code = runC(cmd)
    check(code == 0, 'error: ploy c compilation failed: {!r}', cmd)

  elif target == 'js':
    with open(tmp_path, 'w') as f:
      emit.emit_program(f, modules, main_obj)
    cmd = ['chmod', '+x', tmp_path]
    code = runC(cmd)
    checkF(code == 0, 'error: chmod failed: {!r}', cmd)
  else:
    raiseF("unknown target: '{}'", target)


  move_file(tmp_path, out_path, overwrite=True)


check_module() # python type check.

if __name__ == '__main__':
  main()
