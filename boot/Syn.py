# © 2015 George King.
# Permission to use this file is granted in ploy/license.txt.

import sys
from Src import *


# syntax node.
# last is the position of the last visible character of syntax;
# end is the past-the-end position, including trailing whitespace.
def_struct('Syn',
  "kind:Str src:Src pos:Pos last:Pos end:Pos val:Obj=None subs:Array(Fwd('Syn'))=()")


def syn_has_space(syn:Syn) -> Bool:
  return pos_dist(syn.last, syn.end) > 1


def write_Syn(file:Obj, syn:Syn, depth:Int=0):
  'output a readable dump of a Syn tree.'
  assert isinstance(syn, Syn)
  kind, src, pos, last, end, val, subs = syn
  if pos.line == last.line:
    end_str = '' if pos.col == last.col else '-{}'.format(last.col + 1)
  else:
    end_str = '-{}:{}'.format(last.line + 1, last.col + 1)
  write(file, ' ' * depth * 2, kind, ' ', pos.line + 1, ':', pos.col + 1, end_str)
  if not val is None: # leaf.
    assert subs is ()
    write(file, ' ', repr(val), '\n')
  else: # branch.
    assert not isinstance(subs, Syn)
    write(file, '\n')
    for s in subs:
      write_Syn(file, s, depth + 1)


def log_Syn(syn:Syn):
  write_Syn(stderr, syn)


def fail_syn(syn:Syn, fmt:Str, *items:Obj):
  kind, src, pos, last, end, val, subs = syn
  log_src_pos(src, pos, last, 'error', fmt, *items)
  sys.exit(1)


def fail_syn2(syn:Syn, syn1:Syn, fmt:Str, msg1:Str, *items:Obj):
  kind, src, pos, last, end, val, subs = syn
  log_src_pos(src, pos, last, 'error', fmt, *items)
  kind, src, pos, last, end, val, subs = syn1
  log_src_pos(src, pos, last, 'note', msg1)
  sys.exit(1)


def check_syn(syn:Syn, condition:Bool, fmt:Str, *items:Obj):
  if not condition:
    fail_syn(syn, fmt, *items)


check_module()
