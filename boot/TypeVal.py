# © 2015 George King.
# Permission to use this file is granted in ploy/license.txt.

from sem import *


_TypeVal = Fwd('TypeVal')
def_struct('TVPar', 'name:Opt(Str) type:_TypeVal dflt:Opt(ValExpr)')
def_struct('TVEnum',    'name:Str variants:Array(TVPar)')
def_struct('TVPrim',    'name:Str')
def_struct('TVSig',     'par:_TypeVal ret:_TypeVal')
def_struct('TVStruct',  'name:Str fields:Array(TVPar)')
def_struct('TVTup',     'pars:Array(TVPar)')
def_struct('TVVar',     'name:Str')

TypeVal = Union(
  TVEnum,
  TVPrim,
  TVSig,
  TVStruct,
  TVTup,
  TVVar,
  name='TypeVal')


def vtComparePar(a:TVPar, b:TVPar) -> Bool:
  return vtCompare(a.type, b.type)

def vtCompare(a:TypeVal, b:TypeVal) -> Int:
  if isinstance(a, TVTup) and isinstance(b, TVTup):
    return all(vtCompare(*p) for p in zip(a.pars, b.pars))
  elif isinstance(a, TVVar) and isinstance(b, TVVar):
    return a.name == b.name
  else:
    return a is b


check_module()
