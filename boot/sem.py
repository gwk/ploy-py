# © 2015 George King.
# Permission to use this file is granted in ploy/license.txt.

from Syn import *


_TypeExpr = Fwd('TypeExpr')
_ValExpr = Fwd('ValExpr')
_Stmt = Fwd('Stmt')

def_struct('Sym', 'syn:Syn name:Str')

# Par.

def_struct('DfltPar', 'syn:Syn name:Sym dflt:^ValExpr')
def_struct('NamedPar', 'syn:Syn name:Sym type:^TypeExpr')
# TODO: variad.
Par = Union(
  DfltPar,
  NamedPar,
  _TypeExpr,
  name='Par')

# TypeExpr.

def_struct('Reify', 'syn:Syn callee:^TypeExpr arg:^TypeExpr')
def_struct('Sig',   'syn:Syn par:^TypeExpr ret:^TypeExpr')
def_struct('Tup',   'syn:Syn fields:Array(Par)')

TypeExpr = Union(
  Reify,
  Sig,
  Sym,
  Tup,
  name='TypeExpr')

# ValExpr component types.
def_struct('Arg', 'syn:Syn name:Opt(Sym) expr:^ValExpr') # TODO: variad.
def_struct('Body', 'stmts:Array(_Stmt) tail:Opt(_ValExpr)')
def_struct('Case', 'syn:Syn condition:^ValExpr consequence:^ValExpr')

# ValExpr.

def_struct('Acc',     'syn:Syn accessor:Sym expr:^ValExpr')
def_struct('Agg',     'syn:Syn fields:Array(Arg)')
def_struct('Ann',     'syn:Syn expr:^ValExpr type:TypeExpr')
def_struct('Call',    'syn:Syn callee:^ValExpr arg:^ValExpr')
def_struct('Do',      'syn:Syn body:Body')
def_struct('Fn',      'syn:Syn sig:TypeExpr body:Body')
def_struct('If',      'syn:Syn cond:^ValExpr cases:Array(Case) dflt:^ValExpr')
def_struct('Scoped',  'syn:Syn expr:^ValExpr)')

ValExpr = Union(
  Acc,
  Agg,
  Ann,
  Call,
  Do,
  Fn,
  Int,
  If,
  Scoped,
  Str,
  Sym,
  name='ValExpr')


# Stmt.

def_struct('Alias',   'syn:Syn name:Sym type:TypeExpr')
def_struct('Bind',    'syn:Syn name:Sym expr:ValExpr')
def_struct('Enum',    'syn:Syn name:Sym variants:Array(Par)')
def_struct('Struct',  'syn:Syn name:Sym fields:Array(Par)')

Stmt = Union(
  Alias,
  Bind,
  Call,
  Do,
  Enum,
  If,
  Scoped,
  Struct,
  name='Stmt')

# ModuleStmt.

def_struct('Pub', 'syn:Syn stmt:^ModuleStmt') # strictly, type of expr should exclude Pub.
def_struct('HostDecl', 'syn:Syn name:Sym type:TypeExpr')

ModuleStmt = Union(
  Alias,
  Bind,
  Enum,
  Pub,
  Struct,
  HostDecl,
  name='ModuleStmt')

# modules and main.

def_struct('In',    'syn:Syn name:Sym stmts:Array(ModuleStmt)')
def_struct('Main',  'syn:Syn body:Body')

AnyExpr = Union(TypeExpr, ValExpr, Stmt, ModuleStmt, name='AnyExpr')


check_module()
