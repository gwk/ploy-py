# Copyright 2015 George King.
# Permission to use this file is granted in ploy/license.txt.

from functools import singledispatch

from .fs import *
from .io import *
from .subproc import *
from .typesys import *
from .util import *
